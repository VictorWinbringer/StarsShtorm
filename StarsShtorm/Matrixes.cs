﻿using System.Runtime.InteropServices;
using SharpDX;

namespace StarsShtorm
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Matrixes
    {
        public Matrix World;
        public Matrix View;
        public Matrix Proj;
        public float Size;
        Vector3 _padding0;
        public void Trans()
        {
            World.Transpose();
            View.Transpose();
            Proj.Transpose();
        }
    }
}