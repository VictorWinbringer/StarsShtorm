﻿using SharpDX;
using SharpDX.DXGI;
using SharpDX.WIC;

namespace StarsShtorm
{
    public static class StaticMetods
    {

        /// <summary>
        /// Создает форму в которую будет происходить рендеринг. На ней нужно обязательно вызвать метод Dispose. Форма закрываеть при нажатии Esc. Форма создаеться по размеру экрана и в его центре.
        /// </summary>
        /// <param name="Text">Текст в заголовке формы</param>
        /// <param name="IconFile">Файл в формает .ico для инконки в заголовке формы</param>
        /// <returns></returns>
        public static SharpDX.Windows.RenderForm GetRenderForm(string Text, string iconFile = null)
        {
            if (!SharpDX.Direct3D11.Device.IsSupportedFeatureLevel(SharpDX.Direct3D.FeatureLevel.Level_11_0))
            {
                System.Windows.Forms.MessageBox.Show("Для запуска нужен DirectX 11 ОБЯЗАТЕЛЬНО!");
                return null;
            }
#if DEBUG
            SharpDX.Configuration.EnableObjectTracking = true;
#endif
            var _renderForm = new SharpDX.Windows.RenderForm(Text)
            {
                AllowUserResizing = false,
                IsFullscreen = true,
                StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen,
                ClientSize = new System.Drawing.Size(1200, 900),
                //   ClientSize = new System.Drawing.Size(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height),
                //   FormBorderStyle = System.Windows.Forms.FormBorderStyle.None,
                Icon = iconFile != null ? new System.Drawing.Icon(iconFile) : new System.Drawing.Icon("LogoVW.ico")
            };

            _renderForm.Shown += (sender, e) => { _renderForm.Activate(); };
            _renderForm.KeyDown += (sender, e) => { if (e.KeyCode == System.Windows.Forms.Keys.Escape) _renderForm.Close(); };
            return _renderForm;
        }
        
        /// <summary>
        /// Заргуражет писели из картинки
        /// </summary>
        /// <param name="device">Устройство с помощью которого будет рисоваться эта картинка</param>
        /// <param name="filename">Путь к файлу с картинкой</param>
        /// <returns>Набор пикселей</returns>
        public static BitmapSource LoadBitmapSource(string filename)
        {
            var pFormat = SharpDX.WIC.PixelFormat.Format32bppPRGBA;
            string ext = System.IO.Path.GetExtension(filename);
            if (ext.ToLower() == ".dds") pFormat = SharpDX.WIC.PixelFormat.Format32bppRGBA;
            using (var Imgfactory = new ImagingFactory2())
            using (var d = new BitmapDecoder(
                Imgfactory,
                filename,
                DecodeOptions.CacheOnDemand
            ))
            using (var frame = d.GetFrame(0))
            {
                var fconv = new FormatConverter(Imgfactory);
                fconv.Initialize(
                    frame,
                    pFormat,
                    BitmapDitherType.None, null,
                    0.0, BitmapPaletteType.Custom);
                return fconv;
            }
        }

        public static SharpDX.Direct3D11.Texture2D CreateTex2DFromFile(SharpDX.Direct3D11.DeviceContext device, string filename)
        {
            var bSource = LoadBitmapSource(filename);
            return CreateTex2DFromBitmap(device, bSource);
        }

        public static SharpDX.Direct3D11.Texture2D CreateTex2DFromBitmap(SharpDX.Direct3D11.DeviceContext device, BitmapSource bsource)
        {

            SharpDX.Direct3D11.Texture2DDescription desc;
            desc.Width = bsource.Size.Width;
            desc.Height = bsource.Size.Height;
            desc.ArraySize = 1;
            desc.BindFlags = SharpDX.Direct3D11.BindFlags.ShaderResource;
            desc.Usage = SharpDX.Direct3D11.ResourceUsage.Default;
            desc.CpuAccessFlags = SharpDX.Direct3D11.CpuAccessFlags.None;
            desc.Format = Format.R8G8B8A8_UNorm;
            desc.MipLevels = 1;
            desc.OptionFlags = SharpDX.Direct3D11.ResourceOptionFlags.None;
            desc.SampleDescription.Count = 1;
            desc.SampleDescription.Quality = 0;

            var s = new DataStream(bsource.Size.Height * bsource.Size.Width * 4, true, true);
            bsource.CopyPixels(bsource.Size.Width * 4, s);

            var rect = new DataRectangle(s.DataPointer, bsource.Size.Width * 4);

            var t2D = new SharpDX.Direct3D11.Texture2D(device.Device, desc, rect);
            return t2D;
        }

        /// <summary>
        /// Создает текстуру для шейдера
        /// </summary>
        /// <param name="device">Контекст Директ Икс 11</param>
        /// <param name="filename">Путь к файлу картинки</param>
        /// <returns> Текстуру готовую для использования в шейдере</returns>
        public static SharpDX.Direct3D11.ShaderResourceView LoadTextureFromFile(SharpDX.Direct3D11.DeviceContext device, string filename)
        {
            return new SharpDX.Direct3D11.ShaderResourceView(device.Device, CreateTex2DFromFile(device, filename));
        }
     
    }
}