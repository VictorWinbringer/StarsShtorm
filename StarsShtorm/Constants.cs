﻿using System.Runtime.InteropServices;
using SharpDX;

namespace StarsShtorm
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Constants
    {
        public int GroupDim;
        public uint MaxParticles;
        public float DeltaTime;
        float padding0;
        public Vector3 Atractor;
        float padding1;
    }
}