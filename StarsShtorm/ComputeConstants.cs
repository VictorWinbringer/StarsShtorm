﻿using SharpDX;

namespace StarsShtorm
{
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    internal struct ComputeConstants
    {
        public float Intensity;
        public Vector3 _padding0;
    }
}