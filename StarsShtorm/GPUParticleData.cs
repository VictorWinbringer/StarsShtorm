﻿using System.Runtime.InteropServices;
using SharpDX;

namespace StarsShtorm
{
    [StructLayout(LayoutKind.Sequential)]
    public struct GPUParticleData
    {
        public Vector3 Position;
        public Vector3 Velocity;
    }
}